From ec960a519299c168cde707d93ca72908fd7ccdeb Mon Sep 17 00:00:00 2001
From: Dor Askayo <dor.askayo@gmail.com>
Date: Thu, 25 May 2023 15:17:58 +0300
Subject: [PATCH] Remove libSPIRV-Tools-shared.so

spirv-tools's attempt to allow shared and static build options for
the project is very much broken. See:
https://github.com/KhronosGroup/SPIRV-Tools/issues/3909

The only way to build it without installing shared libraries is by
patching out libSPIRV-Tools-shared.so from CMakeLists.txt.
---
 CMakeLists.txt        |  3 +--
 source/CMakeLists.txt | 22 +++-------------------
 2 files changed, 4 insertions(+), 21 deletions(-)

diff --git a/CMakeLists.txt b/CMakeLists.txt
index 75830b44..6f9625a2 100644
--- a/CMakeLists.txt
+++ b/CMakeLists.txt
@@ -376,7 +376,7 @@ add_custom_target(spirv-tools-pkg-config ALL
                       -DSPIRV_LIBRARIES=${SPIRV_LIBRARIES}
                       -P ${CMAKE_CURRENT_SOURCE_DIR}/cmake/write_pkg_config.cmake
         DEPENDS "CHANGES" "cmake/SPIRV-Tools.pc.in" "cmake/write_pkg_config.cmake")
-add_custom_target(spirv-tools-shared-pkg-config ALL
+add_custom_target(spirv-tools-shared-pkg-config
         COMMAND ${CMAKE_COMMAND}
                       -DCHANGES_FILE=${CMAKE_CURRENT_SOURCE_DIR}/CHANGES
                       -DTEMPLATE_FILE=${CMAKE_CURRENT_SOURCE_DIR}/cmake/SPIRV-Tools-shared.pc.in
@@ -393,7 +393,6 @@ if (ENABLE_SPIRV_TOOLS_INSTALL)
   install(
     FILES
       ${CMAKE_CURRENT_BINARY_DIR}/SPIRV-Tools.pc
-      ${CMAKE_CURRENT_BINARY_DIR}/SPIRV-Tools-shared.pc
     DESTINATION
       ${CMAKE_INSTALL_LIBDIR}/pkgconfig)
 endif()
diff --git a/source/CMakeLists.txt b/source/CMakeLists.txt
index acfa0c12..d600cd25 100644
--- a/source/CMakeLists.txt
+++ b/source/CMakeLists.txt
@@ -378,35 +378,19 @@ function(spirv_tools_default_target_options target)
   add_dependencies(${target} spirv-tools-build-version core_tables enum_string_mapping extinst_tables)
 endfunction()
 
-# Always build ${SPIRV_TOOLS}-shared. This is expected distro packages, and
-# unlike the other SPIRV_TOOLS target, defaults to hidden symbol visibility.
-add_library(${SPIRV_TOOLS}-shared SHARED ${SPIRV_SOURCES})
-spirv_tools_default_target_options(${SPIRV_TOOLS}-shared)
-set_target_properties(${SPIRV_TOOLS}-shared PROPERTIES CXX_VISIBILITY_PRESET hidden)
-target_compile_definitions(${SPIRV_TOOLS}-shared
-  PRIVATE SPIRV_TOOLS_IMPLEMENTATION
-  PUBLIC SPIRV_TOOLS_SHAREDLIB
-)
-
 if(SPIRV_TOOLS_BUILD_STATIC)
   add_library(${SPIRV_TOOLS}-static STATIC ${SPIRV_SOURCES})
   spirv_tools_default_target_options(${SPIRV_TOOLS}-static)
   # The static target does not have the '-static' suffix.
   set_target_properties(${SPIRV_TOOLS}-static PROPERTIES OUTPUT_NAME "${SPIRV_TOOLS}")
 
-  # Create the "${SPIRV_TOOLS}" target as an alias to either "${SPIRV_TOOLS}-static"
-  # or "${SPIRV_TOOLS}-shared" depending on the value of BUILD_SHARED_LIBS.
-  if(BUILD_SHARED_LIBS)
-    add_library(${SPIRV_TOOLS} ALIAS ${SPIRV_TOOLS}-shared)
-  else()
-    add_library(${SPIRV_TOOLS} ALIAS ${SPIRV_TOOLS}-static)
-  endif()
+  add_library(${SPIRV_TOOLS} ALIAS ${SPIRV_TOOLS}-static)
 
-  set(SPIRV_TOOLS_TARGETS ${SPIRV_TOOLS}-static ${SPIRV_TOOLS}-shared)
+  set(SPIRV_TOOLS_TARGETS ${SPIRV_TOOLS}-static)
 else()
   add_library(${SPIRV_TOOLS} ${SPIRV_TOOLS_LIBRARY_TYPE} ${SPIRV_SOURCES})
   spirv_tools_default_target_options(${SPIRV_TOOLS})
-  set(SPIRV_TOOLS_TARGETS ${SPIRV_TOOLS} ${SPIRV_TOOLS}-shared)
+  set(SPIRV_TOOLS_TARGETS ${SPIRV_TOOLS})
 endif()
 
 if("${CMAKE_SYSTEM_NAME}" STREQUAL "Linux")
-- 
2.40.1

